package com.example.quiz;

public class Question
{
    public String questions[] = {
            "Какой геопортал подходит для создания GeoAR?",
            "В какой ГИС реализованна худшая система 3D?",
            "Второй по старшинству университет Москвы?"
    };

    public String choices[][] = {
            {"Mapbox", "ArcGIS Online", "GEOSS", "INSPIRE"},
            {"QGIS", "Панорама", "Нева", "ArcGIS"},
            {"МГУ", "ГУЗ", "МГПУ", "МИИГАиК"}
    };

    public String correctAnswer[] = {
            "Mapbox",
            "Панорама",
            "МИИГАиК"
    };

    public String getQuestion(int a){
        String question = questions[a];
        return question;
    }

    public String getchoice1(int a){
        String choice = choices[a][0];
        return choice;
    }

    public String getchoice2(int a){
        String choice = choices[a][1];
        return choice;
    }

    public String getchoice3(int a){
        String choice = choices[a][2];
        return choice;
    }

    public String getchoice4(int a){
        String choice = choices[a][3];
        return choice;
    }

    public String getCorrectAnswer(int a){
        String answer = correctAnswer[a];
        return answer;
    }
}
